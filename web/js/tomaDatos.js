$(document).ready(function(){
   $('#prioritariosA').submit(function(){
      $.ajax({
          url:'prioritariosAtendidos',
          type: 'POST',
          datatype:'json',
          data:$('#prioritariosA').serialize(),
          success:function(data){
            $('#resultado').html(data);
            $('#sampleTable').DataTable();
          }
      });
      return false;
   });
});
$(document).ready(function(){
   $('#prioritariosNA').submit(function(){  
      $.ajax({
          url:'prioritariosNoAtendidos',
          type: 'POST',
          datatype:'json',
          data:$('#prioritariosNA').serialize(),
          success:function(data){
            $('#resultado').html(data);
            $('#sampleTable').DataTable();
            
          }
      });
      return false;
   });
});

$(document).ready(function(){
   $('#noPrioritariosA').submit(function(){
      $.ajax({
          url:'noPrioritariosAtendidos',
          type: 'POST',
          datatype:'json',
          data:$('#noPrioritariosA').serialize(),
          success:function(data){
            $('#resultado').html(data);
            $('#sampleTable').DataTable();
          }
      });
      return false;
   });
});
$(document).ready(function(){
   $('#noPrioritariosNA').submit(function(){
      $.ajax({
          url:'noPrioritariosNoAtendidos',
          type: 'POST',
          datatype:'json',
          data:$('#noPrioritariosNA').serialize(),
          success:function(data){
            $('#resultado').html(data);
            $('#sampleTable').DataTable();
          }
      });
      return false;
   });
});
//*  *//
$(document).ready(function(){
   $('#siatendio').submit(function(){
      $.ajax({
          url:'siatendido',
          type: 'POST',
          datatype:'json',
          data:$('#siatendio').serialize(),
          success:function(data){
            $('#tit-turno').html(data);
          }
      });
      return false;
   });
});

$(document).ready(function(){
   $('#noatendio').submit(function(){
      $.ajax({
          url:'noatendido',
          type: 'POST',
          datatype:'json',
          data:$('#noatendio').serialize(),
          success:function(data){
            $('#tit-turno').html(data);
          }
      });
      return false;
   });
});

$(document).ready(function(){
   $('#asigturn').submit(function(){
      $.ajax({
          url:'dataTurno',
          type: 'POST',
          datatype:'json',
          data:$('#asigturn').serialize(),
          success:function(data){
              $('#modal-body').html('<h1>'+data+'</h1>');
          }
      });
      return false;
   });
});

$(document).ready(function(){
   $('#asigturn2').submit(function(){
      $.ajax({
          url:'dataTurno',
          type: 'POST',
          datatype:'json',
          data:$('#asigturn2').serialize(),
          success:function(data){
              datos=data;
              $('#modal-body').html('<h1>'+data+'</h1>');
              
          }
      });
      return false;
   });
});

$(document).ready(function(){
   $('#atender').submit(function(){
      $.ajax({
          url:'atenderUsuario',
          type: 'GET',
          datatype:'json',
          data:$('#atender').serialize(),
          success:function(data){
            $('#tit-turno').html(data);
          }
      });
      return false;
   });
});

$(document).ready(function(){
   $('#si').submit(function(){
      $.ajax({
          url:'siatendido',
          type: 'GET',
          datatype:'json',
          data:$('#si').serialize(),
          success:function(data){
            $('#tit-turno').html(data);
          }
      });
      return false;
   });
});



$(document).ready(function(){
    $('#score').load('tbPrioritario.jsp').fadeIn("slow");
    $('#normalesTabla').load('tbNormal.jsp').fadeIn("slow");
    $("#atender").click(function(){
        $('#score').load('tbPrioritario.jsp').fadeIn("slow");
        $('#normalesTabla').load('tbNormal.jsp').fadeIn("slow");
        $("#atendido").show();
        $("#noatendido").show();
        $("#atender").hide();
    });
    $("#atendido").click(function(){
        $("#atendido").hide();
        $("#noatendido").hide();
        $("#atender").show();
    });
    $("#noatendido").click(function(){
        $("#atendido").hide();
        $("#noatendido").hide();
        $("#atender").show();
    });
    
    
});

