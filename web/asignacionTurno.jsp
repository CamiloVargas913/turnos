<%-- 
    Document   : asignacionTurno
    Created on : 25/10/2018, 02:56:29 PM
    Author     : kmilo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Todo Turno</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="images/shards-dashboards-logo.png" />
        <link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <script src="bootstrap-4.1.3-dist/js/jquery-3.3.1.min.js"></script>
       <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>-->
        <script src="bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
        <script src="js/tomaDatos.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css"/>           
    </head>
    <body>
        <section class="container">
            <section class="row bg-info mt-5 rounded">
                <section class="col-12 text-center">
                    <section class="mb-3" >
                     <h1 class="titulo">Todo Turno</h3>
                     <h4 class="text-dark">| Seleccione un tipo de turno |</h4>
                    </section>
                    <form  id="asigturn">
                        <section class="col-xl-12 mb-5">
                            <input type="text" class="d-none" value="N" name="A">
                            <button type="submit" class="asig btn btn-light" data-toggle="modal" data-target="#exampleModal" >Asignar Turno <i class="fa fa-group"></i></button>
                        </section>
                    </form>
                    <form  id="asigturn2">
                        <section class="col-xl-12 mb-5">
                            <input type="text" class="d-none" value="P" name="B">
                            <button type="submit" class="asig btn btn-light" data-toggle="modal" data-target="#exampleModal" >Asignar Turno <i class="fa fa-wheelchair"></i></button>
                        </section>
                    </form>  
                    
                </section>
            </section> 
        </section>
        <!-- Modal -->
        <div class="modal-auto-clear modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Turno Asignado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
                <div class="modal-body" id="modal-body">
                  
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
    </body>
    <!-- cerra modal despues del tiempo-->
    <script>
    $('.modal-auto-clear').on('shown.bs.modal', function () {
    $(this).delay(7000).fadeOut(50, function () {
        $(this).modal('hide');
        });
    });
    </script>
</html>

