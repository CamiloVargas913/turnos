<%
    try{
        HttpSession sessi = request.getSession();
  String usuario=sessi.getAttribute("usuario").toString();
  String id=sessi.getAttribute("id").toString();
  if (usuario=="") {
          out.println("Sesion terminada");
      }else{
  
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js h-100" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Tu Turno</title>
    <meta name="description" content="A high-quality &amp;">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="images/shards-dashboards-logo.png" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.1.0" href="css/styles/shards-dashboards.1.1.0.min.css">
    <link rel="stylesheet" href="css/styles/jquery.dataTables.min.css"/>
  </head>
  <body class="h-100">
    <div class="container-fluid">
      <div class="row">
        <!-- Main Sidebar -->
        <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
          <div class="main-navbar">
            <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
              <a class="navbar-brand w-100 mr-0" href="#" style="line-height: 25px;">
                <div class="d-table m-auto">
                  <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 25px;" src="images/shards-dashboards-logo.svg" alt="Shards Dashboard">
                  <span class="d-none d-md-inline ml-1">Tu Turno</span>
                </div>
              </a>
              <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                  <i class="fas fa-bars"></i>
              </a>
            </nav>
          </div>
          <div class="nav-wrapper">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="reportes.jsp">
                  <i class="fas fa-home"></i>
                  <span>Inicio</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#submenu1">
                    <i class="fa fa-th-list"></i> Reportes
                </a>
                <div class="collapse" id="submenu1" aria-expanded="false">
                    <ul class="flex-column p-1">
                        <li class="nav-item">
                            <a class="nav-link" href="" data-toggle="collapse" data-target="#submenu2">
                                <i class="fas fa-file-pdf" data-toggle="collapse" data-target="#submenu2"></i> Pacientes atendidos
                            </a>
                        </li>
                        <div class="collapse" id="submenu2" aria-expanded="false">
                            <ul class="flex-column nav p-2">
                                <li class="nav-item">
                                    <a class="nav-link " href="prioritariosAtendidos.jsp">
                                        <i class="fa fa-fw fa-wheelchair"></i> Prioritarios.
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="noPrioritariosAtendidos.jsp">
                                        <i class="fa fa-fw fa-user"></i> No prioritarios.
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </ul>   
                </div>        
                <div class="collapse" id="submenu1" aria-expanded="false">
                    <ul class="flex-column p-1">        
                        <li class="nav-item">
                            <a class="nav-link" href="" data-toggle="collapse" data-target="#submenu3">
                                <i class="fas fa-file-pdf" data-toggle="collapse" data-target="#submenu2"></i> Pacientes no atendidos
                            </a>
                        </li>
                        <div class="collapse" id="submenu3" aria-expanded="false">
                            <ul class="flex-column nav p-2">
                                <li class="nav-item">
                                    <a class="nav-link " href="prioritariosNoAtendidos.jsp">
                                        <i class="fa fa-fw fa-wheelchair"></i> Prioritarios.
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="noPrioritariosNoAtendidos.jsp">
                                        <i class="fa fa-fw fa-user"></i> No prioritarios.
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </ul>
                </div>  
              </li>
            </ul>
          </div>
        </aside>
        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
          <div class="main-navbar sticky-top bg-white">
            <!-- Main Navbar -->
            <nav class="navbar align-items-stretch navbar-light flex-md-nowrap pt-1">
             <ul class="navbar-nav border-left border-right  ml-auto">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button">
                    <h4 class="d-none d-md-inline-block"><i class="fas fa-users"></i><%out.println(usuario);%></h4>
                  </a>
                  <div class="dropdown-menu dropdown-menu-small">
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" href="cerrarSesion">
                      <i class="fas fa-sign-out-alt"></i>Salir </a>
                  </div>
                </li>
              </ul>
              <nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="fas fa-bars"></i>
                </a>
              </nav>
            </nav>
          </div>
          <!-- / .main-navbar -->
          <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title"> Atendidos Por Fecha</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Small Stats Blocks -->
            <div class="row">
              <div class="col-lg col-md-12 col-sm-12 mb-12">
                <div class="card p-3">
                    <form id="prioritariosNA"  >
                        <label> <b>Fecha:</b></label>
                        <div class="container">
                            <div class="row">
                              <div class="col-10">
                                  <input type="date" class="form-control" name="fecha">
                              </div>
                              <div class="col-2">
                                  <button type="submit" class="btn btn-dark"><i class="fas fa-search"></i> Buscar</button>
                              </div>

                            </div>
                        </div>
                    </form>
                </div>   
                <div class="stats-small stats-small--1 card card-small mt-3 p-3">
                    <div class="container m-2">
                        <div class="row">
                          <div class="col-10">
                          </div>
                          <div class="col-1" text="align-center">
                              <button type="button" id="reportePNA" class="btn btn-dark"><i class="fas fa-file-pdf"></i> Exportar a PDF</button>
                          </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered" id="sampleTable">
                        <thead>
                          <tr>
                            <th class="text-center" scope="col" COLSPAN="4">Prioritarios atendidos</th>
                          </tr>
                            <tr>
                            <th>Turno</th>
                            <th>Fecha</th>
                            <th>Hora</th>
                            <th>Caja</th>
                          </tr>
                        </thead>
                        <tbody id="resultado">

                        </tbody>
                    </table>
                </div>
              </div>
            </div>
            <!-- End Small Stats Blocks -->
            </div>
          </div>
          <footer class="main-footer d-flex p-2 px-3 bg-white border-top">
            <span class="copyright ml-auto my-auto mr-2">Copyright © 2018
              <a href="" rel="nofollow">Udec</a>
            </span>
          </footer>
        </main>
      </div>
    <script src="bootstrap-4.1.3-dist/js/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script src="https://unpkg.com/shards-ui@latest/dist/js/shards.min.js"></script>
    <script src="js/scripts/extras.1.1.0.min.js"></script>
    <script src="js/scripts/shards-dashboards.1.1.0.min.js"></script>
    <script src="js/tomaDatos.js"></script>
    <script src="js/reportes.js"></script>
    <script type="text/javascript" src="js/scripts/jquery.dataTables.min.js"></script>
  </body>
</html>
       

<%
  }
 }catch (Exception ex) {
            out.println("Error 404");
        }
%>
