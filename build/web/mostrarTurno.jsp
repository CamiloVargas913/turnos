<%-- 
    Document   : asignacionTurno
    Created on : 25/10/2018, 02:56:29 PM
    Author     : kmilo
--%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="conexion.Clases"%>
<%@page import="java.sql.SQLException"%>
<%@page import="conexion.conexiondb"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Todo Turno</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="images/shards-dashboards-logo.png" />
        <link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <script src="bootstrap-4.1.3-dist/js/jquery-3.3.1.min.js"></script>
       <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>-->
        <script src="bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
        <script src="js/tomaDatos.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css"/>           
    </head>
    <body>
        <section class="container">
            <section class="row bg-info mt-5 rounded">
                <section class="col-12 text-center">
                    <section class="mb-3" >
                     <h1 class="titulo">Todo Turno</h1>
                    </section>
                    <div class="card b-white">
                        
                    <table class="table" style="font-size:50px; font-family: candara;">
                                <thead class="thead-light">
                                  <tr>
                                    <th scope="col"><h2>TURNO</h2></th>
                                    <th scope="col"><h2>MODULO</h2></th>
                                  </tr>
                                </thead>
                     <%
                                        Connection con=null;
                                        ResultSet rs=null ,rs2=null,rs3=null;
                                        Statement stm =null;
                                        
                                        conexiondb cone;
                                        try {
                                            cone = new conexiondb();
                                            con=cone.getCon();
                                            stm = con.createStatement();

                                    rs3=stm.executeQuery("SELECT turno_pri,nombre_caja FROM prioritario,caja WHERE estado_pri = '2'  AND prioritario.id_caja = caja.id_caja");
                                    while(rs3.next()){
                                     %>
                                            <tr>
                                              <td scope="row" id="ver"><%="P"+rs3.getString("turno_pri")%></td>
                                              <td id="ver"><%=rs3.getString("nombre_caja")%></td>    
                                          </tr>
                                           <% 
                                        }

                                        rs2=stm.executeQuery("SELECT turno_nor ,nombre_caja FROM normal,caja WHERE estado_nor='2' AND normal.id_caja = caja.id_caja");
                                        while(rs2.next()){
                                        %>
                                            <tr>
                                              <td scope="row" id="ver"><%="N"+rs2.getString("turno_nor")%></td>
                                              <td id="ver"><%=rs2.getString("nombre_caja")%></td>    
                                          </tr>
                                        <% 
                                            }
                                        cone.cerrarConexionDB();

                                        } catch (SQLException ex) {
                                               Logger.getLogger(Clases.class.getName()).log(Level.SEVERE, null, ex);
                                        }  
                                    %>
                   
                       </tbody>
                      </table>
                    </div>    
                   <br>
                   <br>
                </section>
            </section> 
        </section>
        
    </body>
   
<script type="text/javascript">
	var int=self.setInterval("refresh()",1000);
	function refresh()
	{
		location.reload(true);
	}
</script>
   
</html>

