<%-- 
    Document   : cajas
    Created on : 25/10/2018, 04:41:33 PM
    Author     : kmilo
--%>
<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="conexion.Clases"%>
<%@page import="java.sql.SQLException"%>
<%@page import="conexion.conexiondb"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>
<%
    try{
        HttpSession sessi = request.getSession();
  String usuario=sessi.getAttribute("usuario").toString();
  String id=sessi.getAttribute("id").toString();
  if (usuario=="") {
          out.println("Sesion terminada");
      }else{
  
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cajas</title>
        <link rel="stylesheet" href="bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <link rel="icon" type="image/png" href="images/shards-dashboards-logo.png" />
        <script src="bootstrap-4.1.3-dist/js/jquery-3.3.1.min.js"></script>
       <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>-->
        <script src="bootstrap-4.1.3-dist/js/bootstrap.min.js"></script>
        <script src="js/tomaDatos.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css"/> <script src="http://code.jquery.com/jquery-latest.js"></script>
</head>

    </head>
    <body>
        <nav class="navbar navbar-expand navbar-dark bg-info">
            <a class="navbar-brand" href="#">Turnos</a>
            <div class="navbar-collapse collapse align-content-lg-end ">
                <ul class="navbar-nav ml-auto pt-1">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-white" data-toggle="dropdown" id="Preview" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <%out.println(usuario);%><i class="fa fa-inbox"></i></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="Preview">
                        <a class="dropdown-item" href="cerrarSesion">Salir <i class="fa fa-sign-out"></i></a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <section class="container">
            <section class="row bg-info ">
                <section class="col-12 text-center">
                    <section class="mb-3" >
                     <h1 class="titulo">Cajas</h3>
                     <h4 class="text-dark">| Atiende Usuarios |</h4>
                    </section>
                    <section class="card p-1 text-dark col-sm-12">
                        <h3 id="tit-turno">Turno:</h3>
                    </section>
                    <div class="row pt-3">
                        <div class="col-sm-6">
                          <div class="card">
                            <div class="card-header">
                               <h5 class="card-title"> Prioritarios <i class="fa fa-wheelchair"></i></h5>
                            </div>
                            <div class="card-body" id="score">
                              
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="card">
                            <div class="card-header">
                               <h5 class="card-title"> No Prioritarios <i class="fa fa-group"></i></h5>
                            </div>
                            <div class="card-body" id="normalesTabla">
                              
                            </div>
                          </div>
                        </div>
                    </div>
                    <form id="atender">
                        <input  class="d-none"  name="id" value="<%out.println(id);%>">
                        <button type="submit" class=" aten btn btn-light btn-lg" id="atender">Atender <i class="fa fa-group"></i></button> 
                    </form>           
                    
                    <form id="siatendio">
                        <input  class="d-none"  name="id" value="<%out.println(id);%>">
                        <button type="submit" class=" aten btn btn-light btn-lg" id="atendido">Atendido <i class="fa fa-group"></i></button>
                    </form>
                    
                    <form id="noatendio">
                        <input  class="d-none"  name="id" value="<%out.println(id);%>">
                        <button type="submit" class=" aten btn btn-light btn-lg" id="noatendido">No atendido <i class="fa fa-group"></i></button>
                    </form>
                    
                    
                       
                </section>
            </section> 
        </section>
    </body>
  

</html>

<%
  }
 }catch (Exception ex) {
            out.println("Error 404");
        }
%>
