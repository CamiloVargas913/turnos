/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import conexion.Clases;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author david
 */
@WebServlet(name = "noPrioritariosNoAtendidosPDF", urlPatterns = {"/noPrioritariosNoAtendidosPDF"})
public class noPrioritariosNoAtendidosPDF extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/pdf");
        OutputStream out = response.getOutputStream();
        
        try {
            try {
                String fecha=request.getParameter("fecha");
                ResultSet rs=  null;
                Clases reportes = new Clases();
                reportes.Clases();
                rs=reportes.noPrioritariosNoAtendidos(fecha);
                Document documento = new Document(PageSize.CROWN_QUARTO,72,72,72,72);
                PdfWriter.getInstance(documento, out);
                
                documento.open();
                Image imagen=Image.getInstance("C:/Udec/Bases de Datos/Proyecto/turnos/web/imagenes/logo.jpg");
                imagen.scaleAbsoluteWidth(60f);
                imagen.scaleAbsoluteHeight(40f);
                documento.add(imagen);
                Paragraph titulo = new Paragraph();
                Font fontTitulo=new Font(Font.FontFamily.HELVETICA,16,Font.BOLD,BaseColor.BLUE);
                titulo.add(new Phrase("Reporte pacientes no prioritarios no atendidos", fontTitulo));
                titulo.setAlignment(Element.ALIGN_CENTER);
                titulo.add(new Phrase(Chunk.NEWLINE));
                titulo.add(new Phrase(Chunk.NEWLINE));
                documento.add(titulo);
                
                Paragraph texto = new Paragraph();
                Font fontTexto=new Font(Font.FontFamily.HELVETICA,12,Font.BOLD,BaseColor.BLACK);
                texto.add(new Phrase("En este reporte podemos visualizar los turnos no prioritarios no atendidos en la fecha: "+fecha, fontTexto));
                texto.setAlignment(Element.ALIGN_JUSTIFIED);
                texto.add(new Phrase(Chunk.NEWLINE));
                texto.add(new Phrase(Chunk.NEWLINE));
                documento.add(texto);
                
                PdfPTable tabla = new PdfPTable(4);
                PdfPCell columna1 = new PdfPCell(new Paragraph("Turno",FontFactory.getFont("Arial",12,Font.BOLD,BaseColor.BLACK)));
                PdfPCell columna2 = new PdfPCell(new Paragraph("Fecha",FontFactory.getFont("Arial",12,Font.BOLD,BaseColor.BLACK)));
                PdfPCell columna3 = new PdfPCell(new Paragraph("Hora",FontFactory.getFont("Arial",12,Font.BOLD,BaseColor.BLACK)));
                PdfPCell columna4 = new PdfPCell(new Paragraph("Caja",FontFactory.getFont("Arial",12,Font.BOLD,BaseColor.BLACK)));
                
                tabla.addCell(columna1);
                tabla.addCell(columna2);
                tabla.addCell(columna3);
                tabla.addCell(columna4);
                while(rs.next()){
                    tabla.addCell(rs.getString(1));
                    tabla.addCell(rs.getString(2));
                    tabla.addCell(rs.getString(3));
                    tabla.addCell(rs.getString(4));
                }
                
                documento.add(tabla);
                documento.close();
            } catch (Exception e) {
                
            }
        }finally{
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
