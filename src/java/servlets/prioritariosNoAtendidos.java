/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import conexion.Clases;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author david
 */
@WebServlet(name = "prioritariosNoAtendidos", urlPatterns = {"/prioritariosNoAtendidos"})
public class prioritariosNoAtendidos extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String fecha=request.getParameter("fecha");
            ResultSet rs=  null;
            Clases reportes = new Clases();
            reportes.Clases();
            rs=reportes.prioritariosNoAtendidos(fecha);
            
            String mensaje="No hay resultados";
            List<List<String>> arrayConsulta = new ArrayList<List<String>>();
            try {
                for (int i = 0; i <=4; i++) {
                arrayConsulta.add(new ArrayList<String>());
                }
                while(rs.next()){
                    arrayConsulta.get(0).add(rs.getString("turno_pri"));
                    arrayConsulta.get(1).add(rs.getString("fecha_pri"));
                    arrayConsulta.get(2).add(rs.getString("hora_pri"));
                    arrayConsulta.get(3).add(rs.getString("nombre_caja"));

                } 
                if (arrayConsulta.isEmpty()) {
                    out.println("<tr>");
                    out.println("<th><font color=\"black\">"+"paila"+"</font></th>");
                    out.println("<tr>");
                }else{
                    for (int i = 0; i <= arrayConsulta.get(0).size()-1; i++) {
                    out.println("<tr>");
                    out.println("<th><font color=\"black\">"+arrayConsulta.get(0).get(i)+"</font></th>");
                    out.println("<th><font color=\"black\">"+arrayConsulta.get(1).get(i)+"</font></th>");
                    out.println("<th><font color=\"black\">"+arrayConsulta.get(2).get(i)+"</font></th>");
                    out.println("<th><font color=\"black\">"+arrayConsulta.get(3).get(i)+"</font></th>");
                    out.println("</tr>");
                    }
                }
                
           
            } catch (Exception e) {
            
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
