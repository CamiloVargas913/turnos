/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.*;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.GregorianCalendar;

/**
 *
 * @author kmilo
 */
public class Clases {
Connection con=null;
ResultSet rs=null;
Statement stm =null;
String turno="";
conexiondb cone;
    public void Clases(){
        
        try {
            cone = new conexiondb();
            con=cone.getCon();
            stm = con.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(Clases.class.getName()).log(Level.SEVERE, null, ex);
        }     
            
    } 
    public String asignarTurnoNor(String cate){
        try {
            String estado="1";
            String query = "INSERT INTO public.normal(estado_nor)VALUES ('"+estado+"')";
            int consul = stm.executeUpdate(query);
            
            rs=stm.executeQuery("SELECT * FROM normal ORDER BY id_normal DESC LIMIT 1");
            rs.next();
            turno=rs.getString("turno_nor");
            
            if (consul == 1) {
                 return cate+turno;
                 
            }
            cone.cerrarConexionDB();
        } catch (SQLException ex) {
            Logger.getLogger(Clases.class.getName()).log(Level.SEVERE, null, ex);
        }
         return ""; 
    } 
    
    public String asignarTurnoPri(String cate){
        try {
            String estado="1";
            String query = "INSERT INTO public.prioritario(estado_pri) VALUES ('"+estado+"')";
            int consul = stm.executeUpdate(query);
            
            rs=stm.executeQuery("SELECT * FROM prioritario ORDER BY id_prioritario DESC LIMIT 1");
            rs.next();
            turno=rs.getString("turno_pri");
            
            
            if (consul == 1) {
                 return cate+turno;
            }
            cone.cerrarConexionDB();
             
        } catch (SQLException ex) {
            Logger.getLogger(Clases.class.getName()).log(Level.SEVERE, null, ex);
        }
         return ""; 
    } 


    public String ingresoCajas(String Usuario){    
        try {
            rs=stm.executeQuery("SELECT id_caja FROM public.caja WHERE  nombre_caja = '"+Usuario+"'");
            if (rs.next()== false){
                 return "";
            }else{
                return rs.getString("id_caja");   
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Clases.class.getName()).log(Level.SEVERE, null, ex);
        }
        cone.cerrarConexionDB();
        return "";
    }

    public String atenderUsuPrio(String cajera){
        try {
            rs=stm.executeQuery("SELECT * FROM prioritario WHERE estado_pri = '1' ORDER BY id_prioritario LIMIT 1");      
            if (rs.next()==false) { 
             return "";
            }else{
              turno=rs.getString("turno_pri");   
              String idTurno=rs.getString("id_prioritario");  
              int consul = stm.executeUpdate("UPDATE public.prioritario SET estado_pri='"+2+"', id_caja='"+cajera+"' WHERE id_prioritario='"+idTurno+"'");
                if (consul==1){         
                return "Turno: P"+turno;
                }
            }
            cone.cerrarConexionDB();
        } catch (SQLException ex) {
            Logger.getLogger(Clases.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
    
    public String atenderUsuNor(String cajera){
        try {
            rs=stm.executeQuery("SELECT * FROM normal WHERE estado_nor = '1' ORDER BY id_normal LIMIT 1");      
            if (rs.next()==false) { 
             return "";
            }else{
              turno=rs.getString("turno_nor");   
              String idTurno=rs.getString("id_normal");  
              int consul = stm.executeUpdate("UPDATE public.normal SET estado_nor='"+2+"', id_caja='"+cajera+"' WHERE id_normal='"+idTurno+"'");
                if (consul==1){         
                return "Turno: N"+turno;
                }
            }
            cone.cerrarConexionDB();
        } catch (SQLException ex) {
            Logger.getLogger(Clases.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
    public ResultSet reportes(String fecha){
        System.out.println(fecha);
        try {
            rs=stm.executeQuery("Select normal.turno_nor, normal.fecha_nor, normal.hora_nor, normal.estado_nor from normal where fecha_nor='"+fecha+"'");
            
            cone.cerrarConexionDB();
        } catch (Exception e) {
            
        }
        return rs;
    }
    
    ///////////////botones de atenido //////////////////////////////
    public String siAtendidoPri(String caja){
        
       String horaF = hora();
       String fechaF = fecha();

        String si = "Atendido";
        try {
            rs=stm.executeQuery("SELECT * FROM prioritario WHERE estado_pri = '2' AND id_caja = '"+caja+"' ORDER BY id_prioritario LIMIT 1");      
            if (rs.next()==false) { 
                return "";
            }else{
              turno=rs.getString("turno_pri");   
              String idTurno=rs.getString("id_prioritario");  
              
              int consul = stm.executeUpdate("UPDATE public.prioritario SET estado_pri='"+3+"', fecha_pri='"+fechaF+"' , hora_pri='"+horaF+"' WHERE id_prioritario='"+idTurno+"'");
                if (consul==1){         
                 return si;
                }
            }
            cone.cerrarConexionDB();
        } catch (SQLException ex) {
            Logger.getLogger(Clases.class.getName()).log(Level.SEVERE, null, ex);
        }
       return "";
    }
    public String siAtendidoNor(String caja){
        String si = "Atendido";
        String horaF = hora();
        String fechaF = fecha();
        
        try {
            rs=stm.executeQuery("SELECT * FROM normal WHERE estado_nor = '2' AND id_caja = '"+caja+"' ORDER BY id_normal LIMIT 1");      
            if (rs.next()==false) { 
                return "";
            }else{
              turno=rs.getString("turno_nor");   
              String idTurno=rs.getString("id_normal");  
              
              int consul = stm.executeUpdate("UPDATE public.normal SET estado_nor='"+3+"', fecha_nor='"+fechaF+"' , hora_nor='"+horaF+"' WHERE id_normal='"+idTurno+"'");
                if (consul==1){         
                 return si;
                }
            }
            cone.cerrarConexionDB();
        } catch (SQLException ex) {
            Logger.getLogger(Clases.class.getName()).log(Level.SEVERE, null, ex);
        }
       return "";
    }
    ///////////////botones no atenido //////////////////////////////
    public String noAtendidoPri(String caja){
        String si = "No se atendido";
        String horaF = hora();
        String fechaF = fecha();
        
        try {
            rs=stm.executeQuery("SELECT * FROM prioritario WHERE estado_pri = '2' AND id_caja = '"+caja+"' ORDER BY id_prioritario LIMIT 1");      
            if (rs.next()==false) { 
                return "";
            }else{
              turno=rs.getString("turno_pri");   
              String idTurno=rs.getString("id_prioritario");  
              
              int consul = stm.executeUpdate("UPDATE public.prioritario SET estado_pri='"+4+"', fecha_pri='"+fechaF+"' , hora_pri='"+horaF+"' WHERE id_prioritario='"+idTurno+"'");
                if (consul==1){         
                 return si;
                }
            }
            cone.cerrarConexionDB();
        } catch (SQLException ex) {
            Logger.getLogger(Clases.class.getName()).log(Level.SEVERE, null, ex);
        }
       return "";
    }
    public String noAtendidoNor(String caja){
        String si = "No se atendio";
        String horaF = hora();
        String fechaF = fecha();
        try {
            rs=stm.executeQuery("SELECT * FROM normal WHERE estado_nor = '2' AND id_caja = '"+caja+"' ORDER BY id_normal LIMIT 1");      
            if (rs.next()==false) { 
                return "";
            }else{
              turno=rs.getString("turno_nor");   
              String idTurno=rs.getString("id_normal");  
              
              int consul = stm.executeUpdate("UPDATE public.normal SET estado_nor='"+4+"',fecha_nor='"+fechaF+"' , hora_nor='"+horaF+"' WHERE id_normal='"+idTurno+"'");
                if (consul==1){         
                 return si;
                }
            }
            cone.cerrarConexionDB();
        } catch (SQLException ex) {
            Logger.getLogger(Clases.class.getName()).log(Level.SEVERE, null, ex);
        }
       return "";
    }
    
    public String hora(){
        Calendar fecha = new GregorianCalendar();
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);
        
        /*int anio = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);*/
        
        String hora2,minuto2,segundo2,horaF,anio2,mes2,dia2,fechaF;
        
        hora2 = Integer.toString(hora);
        minuto2 = Integer.toString(minuto);
        segundo2 = Integer.toString(segundo);
        
        /*anio2 = Integer.toString(anio);
        mes2 = Integer.toString(mes);
        dia2 = Integer.toString(dia);*/
        
        
        horaF = hora2+":"+minuto2+":"+segundo2;
      //  fechaF = anio2+"-"+mes2+"-"+dia2;
        return horaF;
    }
    
    public String fecha(){
        Calendar fecha = new GregorianCalendar();
       /* int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);*/
        
        int anio = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        
        String hora2,minuto2,segundo2,horaF,anio2,mes2,dia2,fechaF;
        
       /* hora2 = Integer.toString(hora);
        minuto2 = Integer.toString(minuto);
        segundo2 = Integer.toString(segundo);*/
        
        anio2 = Integer.toString(anio);
        mes2 = Integer.toString(mes);
        dia2 = Integer.toString(dia);
        
        
        //horaF = hora2+":"+minuto2+":"+segundo2;
        fechaF = anio2+"-"+mes2+"-"+dia2;
        return fechaF;
    }
    /*cunsultas tablas*/
    public ResultSet prioritariosAtendidos(String fecha){
        System.out.println(fecha);
        try {
            rs=stm.executeQuery("Select prioritario.turno_pri, prioritario.fecha_pri, prioritario.hora_pri, caja.nombre_caja from prioritario,caja where fecha_pri='"+fecha+"' and caja.id_caja=prioritario.id_caja and prioritario.estado_pri='"+3+"'");
            cone.cerrarConexionDB();
        } catch (Exception e) {
            
        }
        return rs;
    }
    public ResultSet prioritariosNoAtendidos(String fecha){
        System.out.println(fecha);
        try {
            rs=stm.executeQuery("Select prioritario.turno_pri, prioritario.fecha_pri, prioritario.hora_pri, caja.nombre_caja from prioritario,caja where fecha_pri='"+fecha+"' and caja.id_caja=prioritario.id_caja and prioritario.estado_pri='"+4+"'");
            cone.cerrarConexionDB();
        } catch (Exception e) {
            
        }
        return rs;
    }
     public ResultSet noPrioritariosNoAtendidos(String fecha){
        System.out.println(fecha);
        try {
            rs=stm.executeQuery("Select normal.turno_nor, normal.fecha_nor, normal.hora_nor, caja.nombre_caja from normal,caja where fecha_nor='"+fecha+"' and caja.id_caja=normal.id_caja and normal.estado_nor='"+4+"'");
            cone.cerrarConexionDB();
        } catch (Exception e) {
            
        }
        return rs;
    }
      public ResultSet noPrioritariosAtendidos(String fecha){
        System.out.println(fecha);
        try {
            rs=stm.executeQuery("Select normal.turno_nor, normal.fecha_nor, normal.hora_nor, caja.nombre_caja from normal,caja where fecha_nor='"+fecha+"' and caja.id_caja=normal.id_caja and normal.estado_nor='"+3+"'");
            cone.cerrarConexionDB();
        } catch (Exception e) {
            
        }
        return rs;
    }
      public String noprioaten(){
       try {
            rs=stm.executeQuery("SELECT count(turno_nor) as total FROM public.normal where estado_nor='3' and fecha_nor=CURRENT_DATE GROUP BY (estado_nor);");
            if (rs.next()== false){
                 return "";
            }else{
                return rs.getString("total");   
            }
        } catch (SQLException ex) {
            Logger.getLogger(Clases.class.getName()).log(Level.SEVERE, null, ex); 
        }
       cone.cerrarConexionDB();
        return "";   
      }
      public String prioaten(){
       try {
            rs=stm.executeQuery("SELECT count(turno_pri) as total FROM public.prioritario where estado_pri='3' and fecha_pri=CURRENT_DATE GROUP BY (estado_pri);");
            if (rs.next()== false){
                 return "";
            }else{
                return rs.getString("total");   
            }
        } catch (SQLException ex) {
            Logger.getLogger(Clases.class.getName()).log(Level.SEVERE, null, ex); 
        }
       cone.cerrarConexionDB();
        return "";   
      }
    
}