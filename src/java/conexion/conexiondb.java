/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kmilo
 */
public class conexiondb {
 private Connection con;
 
    public conexiondb() throws SQLException {
       String user="postgres",contra="camilo1222",db="turnos";
     try {
         Class.forName("org.postgresql.Driver");
         String url= "jdbc:postgresql://localhost:5432/"+db;
         con=DriverManager.getConnection(url,user,contra);
     } catch (ClassNotFoundException ex) {
         Logger.getLogger(conexiondb.class.getName()).log(Level.SEVERE, null, ex);
     }
    }
    
    public void cerrarConexionDB(){
     try {
         con.close();
     } catch (SQLException ex) {
         Logger.getLogger(conexiondb.class.getName()).log(Level.SEVERE, null, ex);
     }
    }

    public Connection getCon() {
        return con;
    }
    
}
